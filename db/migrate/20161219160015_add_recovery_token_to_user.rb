class AddRecoveryTokenToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :recovery_password_token, :string
  end
end
