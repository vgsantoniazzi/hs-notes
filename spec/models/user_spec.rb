# frozen_string_literal: true
require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Validations' do
    subject { User.new }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:email) }

    it { is_expected.to allow_value('vgsantoniazzi@gmail.com').for(:email) }
    it { is_expected.to_not allow_value('vgsantoniazzi.com').for(:email) }
  end

  describe 'Associations' do
    it { is_expected.to have_many(:notes) }
  end

  describe '#generate_bearer_token' do
    let(:user) { create(:user) }

    it do
      expect { user.generate_bearer_token }.to change {
        user.bearer_token
      }.from(nil).to(String)
    end

    it do
      user.generate_bearer_token
      expect(user.bearer_token.size).to eq(32)
    end
  end

  describe '#generate_recovery_password_token' do
    let(:user) { create(:user) }

    it do
      expect { user.generate_recovery_password_token }.to change {
        user.recovery_password_token
      }.from(nil).to(String)
    end

    it do
      user.generate_recovery_password_token
      expect(user.recovery_password_token.size).to eq(4)
    end
  end
end
