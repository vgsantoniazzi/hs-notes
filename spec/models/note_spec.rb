# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Note, type: :model do
  describe 'Validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:text) }
    it { is_expected.to validate_presence_of(:user) }

    it { is_expected.to validate_length_of(:text).is_at_most(320) }
  end

  describe 'Associations' do
    it { is_expected.to belong_to(:user) }
  end
end
