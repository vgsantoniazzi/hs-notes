# frozen_string_literal: true
FactoryGirl.define do
  factory :user do
    name 'John Doe'
    sequence :email do |n|
      "john#{n}@doe.com"
    end
    password '123456'
    bearer_token nil
  end
end
