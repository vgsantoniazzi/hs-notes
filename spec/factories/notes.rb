# frozen_string_literal: true
FactoryGirl.define do
  factory :note do
    title 'Loren ipsum'
    text 'Dolor sit amet'
    user
  end
end
