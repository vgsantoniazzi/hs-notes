# frozen_string_literal: true
require 'rails_helper'

RSpec.describe RecoveryPasswordMailer, type: :mailer do
  describe 'instructions' do
    let(:user) { create(:user, recovery_password_token: 'acD3') }
    let(:mail) { described_class.send_email(user).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('Your recovery password code.')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['noreply@herokuapp.com'])
    end

    it 'assigns @name' do
      expect(mail.body.encoded).to match('<b>acD3</b>')
    end
  end
end
