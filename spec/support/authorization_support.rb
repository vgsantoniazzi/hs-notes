# frozen_string_literal: true
def sign_in(user)
  user.generate_bearer_token unless user.bearer_token
  { 'HTTP_AUTHORIZATION' => "Bearer token=#{user.bearer_token}" }
end
