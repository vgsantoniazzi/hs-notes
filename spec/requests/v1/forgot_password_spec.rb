# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Forgot Password API' do
  describe 'POST #create' do
    let!(:user) { create(:user) }

    it do
      expect do
        post '/api/v1/forgot_password', params: { user: { email: user.email } }
      end.to change { ActionMailer::Base.deliveries.count }.by(1)
    end

    it do
      expect do
        post '/api/v1/forgot_password', params: { user: { email: user.email } }
      end.to change { user.reload.recovery_password_token }.from(nil).to(String)
    end
  end
end
