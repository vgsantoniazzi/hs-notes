# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Users API' do
  describe 'GET #show' do
    let(:user) { create(:user) }

    before do
      get '/api/v1/users/me', headers: sign_in(user)
    end

    it do
      expect(json).to eq(
        'id' => user.id,
        'email' => user.email,
        'name' => user.name,
        'bearer_token' => user.bearer_token
      )
    end
  end

  describe 'POST #create' do
    let(:user_attributes) { attributes_for(:user) }

    context 'success' do
      before do
        post '/api/v1/users', params: { user: user_attributes }
      end

      it do
        expect(json).to eq(
          'id' => json['id'],
          'email' => user_attributes[:email],
          'name' => user_attributes[:name],
          'bearer_token' => json['bearer_token']
        )
      end
    end

    context 'failure' do
      before do
        post '/api/v1/users', params: { user: user_attributes.merge(password: nil) }
      end

      it do
        expect(json).to eq(
          'error' => { 'message' => { 'password' => ["can't be blank"] }, 'class' => 'ModelValidationError', 'status' => 400 }
        )
      end
    end
  end
end
