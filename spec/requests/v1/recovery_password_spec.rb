# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Recovery Password API' do
  describe 'POST #create' do
    let!(:user) { create(:user, recovery_password_token: '3Ad4') }

    context 'success' do
      it do
        expect do
          post '/api/v1/recovery_password', params: { user: { email: user.email, recovery_password_token: '3Ad4', password: '112233' } }
        end.to change { user.reload.password_digest }
      end

      it do
        expect do
          post '/api/v1/recovery_password', params: { user: { email: user.email, recovery_password_token: '3Ad4', password: '112233' } }
        end.to change { user.reload.recovery_password_token }.from(String).to(nil)
      end

      it do
        post '/api/v1/recovery_password', params: { user: { email: user.email, recovery_password_token: '3Ad4', password: '112233' } }

        expect(json).to eq(
          'id' => json['id'],
          'name' => user.name,
          'email' => user.email,
          'bearer_token' => nil
        )
      end
    end

    context 'failure' do
      context 'model validation error' do
        it do
          expect do
            post '/api/v1/recovery_password', params: { user: { email: user.email, recovery_password_token: '3Ad4', password: nil } }
          end.to_not change { user.reload.password_digest }
        end

        it do
          expect do
            post '/api/v1/recovery_password', params: { user: { email: user.email, recovery_password_token: '3Ad4', password: nil } }
          end.to_not change { user.reload.recovery_password_token }
        end

        it do
          post '/api/v1/recovery_password', params: { user: { email: user.email, recovery_password_token: '3Ad4', password: nil } }

          expect(json).to eq(
            'error' => {
              'message' => {
                'password' => ["can't be blank"]
              },
              'class' => 'ModelValidationError',
              'status' => 400
            }
          )
        end
      end

      context 'user not found' do
        it do
          expect do
            post '/api/v1/recovery_password', params: { user: { email: user.email, recovery_password_token: 'not', password: '112233' } }
          end.to_not change { user.reload.password_digest }
        end

        it do
          expect do
            post '/api/v1/recovery_password', params: { user: { email: user.email, recovery_password_token: 'not', password: '112233' } }
          end.to_not change { user.reload.recovery_password_token }
        end

        it do
          post '/api/v1/recovery_password', params: { user: { email: user.email, recovery_password_token: 'not', password: '112233' } }

          expect(json).to eq(
            'error' => {
              'message' => "Couldn't find User",
              'class' => 'ActiveRecord::RecordNotFound',
              'status' => 404
            }
          )
        end
      end
    end
  end
end
