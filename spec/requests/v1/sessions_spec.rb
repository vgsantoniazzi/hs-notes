# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Sessions API' do
  describe 'POST #create' do
    let!(:user) { create(:user) }

    context 'success' do
      it do
        post '/api/v1/sessions', params: { user: { email: user.email, password: '123456' } }
        expect(response.status).to eq(201)
      end

      it do
        post '/api/v1/sessions', params: { user: { email: user.email, password: '123456' } }
        expect(json).to eq(
          'id' => user.id,
          'name' => user.name,
          'email' => user.email,
          'bearer_token' => json['bearer_token']
        )
      end

      it do
        expect do
          post '/api/v1/sessions', params: { user: { email: user.email, password: '123456' } }
        end.to change { user.reload.bearer_token }.from(nil).to(String)
      end
    end

    context 'failure' do
      it do
        post '/api/v1/sessions', params: { user: { email: '', password: '' } }
        expect(response.status).to eq(401)
      end

      it do
        post '/api/v1/sessions', params: { user: { email: user.email, password: '' } }
        expect(json).to eq(
          'error' => {
            'message' => 'Not authorized',
            'class' => 'UnauthorizedError',
            'status' => 401
          }
        )
      end

      it do
        expect do
          post '/api/v1/sessions', params: { user: { email: user.email, password: '' } }
        end.to_not change { user.reload.bearer_token }
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:user) { create(:user) }

    before do
      delete '/api/v1/sessions', headers: sign_in(user)
    end

    it do
      expect(response.status).to eq(204)
    end
  end
end
