# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Rescue Errors' do
  context 'Route Not Found' do
    before do
      get '/obviously_not_found'
    end

    it do
      expect(json).to eq(
        'error' => {
          'message' => 'Endpoint requested not found.',
          'class' => 'RoutingError',
          'status' => 404
        }
      )
    end
  end

  context 'Parameter Missing' do
    before do
      post '/api/v1/users'
    end

    it do
      expect(json).to eq(
        'error' => {
          'message' => 'param is missing or the value is empty: user',
          'class' => 'ActionController::ParameterMissing',
          'status' => 400
        }
      )
    end
  end
end
