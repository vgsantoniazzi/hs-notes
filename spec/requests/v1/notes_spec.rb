# frozen_string_literal: true
require 'spec_helper'

RSpec.describe 'Notes API' do
  let!(:user) { create(:user) }

  context 'GET #index' do
    let!(:note_one) { create(:note, user: user) }
    let!(:note_two) { create(:note) }

    before do
      get '/api/v1/notes', headers: sign_in(user)
    end

    it do
      expect(json).to eq(
        [
          'id' => note_one.id,
          'title' => note_two.title,
          'text' => note_two.text
        ]
      )
    end
  end

  context 'GET #show' do
    let!(:note_one) { create(:note, user: user) }
    let!(:note_two) { create(:note) }

    context 'access own note' do
      before do
        get "/api/v1/notes/#{note_one.id}", headers: sign_in(user)
      end

      it do
        expect(json).to eq(
          'id' => note_one.id,
          'title' => note_two.title,
          'text' => note_two.text
        )
      end
    end

    context 'access others user note' do
      before do
        get "/api/v1/notes/#{note_two.id}", headers: sign_in(user)
      end

      it do
        expect(json).to eq(
          'error' => {
            'message' => "Couldn't find Note with 'id'=#{note_two.id} [WHERE \"notes\".\"user_id\" = $1]",
            'class' => 'ActiveRecord::RecordNotFound',
            'status' => 404
          }
        )
      end
    end
  end

  context 'POST #create' do
    let!(:note_attributes) { attributes_for(:note) }

    context 'success' do
      before do
        post '/api/v1/notes', params: { note: note_attributes }, headers: sign_in(user)
      end

      it do
        expect(json).to eq(
          'id' => json['id'],
          'title' => note_attributes[:title],
          'text' => note_attributes[:text]
        )
      end
    end

    context 'failure' do
      before do
        post '/api/v1/notes', params: { note: note_attributes.merge(title: nil) }, headers: sign_in(user)
      end

      it do
        expect(json).to eq(
          'error' => {
            'message' => {
              'title' => ["can't be blank"]
            },
            'class' => 'ModelValidationError',
            'status' => 400
          }
        )
      end
    end
  end

  context 'PUT #update' do
    let!(:note) { create(:note, user: user) }
    let!(:note_attributes) { attributes_for(:note, title: 'newest cool note') }

    context 'success' do
      before do
        put "/api/v1/notes/#{note.id}", params: { note: note_attributes }, headers: sign_in(user)
      end

      it do
        expect(json).to eq(
          'id' => note.id,
          'title' => note_attributes[:title],
          'text' => note_attributes[:text]
        )
      end
    end

    context 'failure' do
      before do
        put "/api/v1/notes/#{note.id}", params: { note: note_attributes.merge(title: nil) }, headers: sign_in(user)
      end

      it do
        expect(json).to eq(
          'error' => {
            'message' => {
              'title' => ["can't be blank"]
            },
            'class' => 'ModelValidationError',
            'status' => 400
          }
        )
      end
    end
  end

  context 'DELETE #destroy' do
    let!(:note_one) { create(:note, user: user) }
    let!(:note_two) { create(:note) }

    context 'access own note' do
      before do
        delete "/api/v1/notes/#{note_one.id}", headers: sign_in(user)
      end

      it do
        expect(response.status).to eq(204)
      end
    end

    context 'access others user note' do
      before do
        delete "/api/v1/notes/#{note_two.id}", headers: sign_in(user)
      end

      it do
        expect(json).to eq(
          'error' => {
            'message' => "Couldn't find Note with 'id'=#{note_two.id} [WHERE \"notes\".\"user_id\" = $1]",
            'class' => 'ActiveRecord::RecordNotFound',
            'status' => 404
          }
        )
      end
    end
  end
end
