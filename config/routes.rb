# frozen_string_literal: true
Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resource :sessions, only: [:create] do
        collection do
          delete :destroy
        end
      end

      resources :users, only: [:create] do
        collection do
          get :me, to: 'users#show'
          put :update
        end
      end

      resources :notes

      post 'forgot_password', to: 'forgot_password#create'
      post 'recovery_password', to: 'recovery_password#create'
    end
  end

  get    '*a', to: 'errors#routing'
  post   '*a', to: 'errors#routing'
  put    '*a', to: 'errors#routing'
  delete '*a', to: 'errors#routing'
end
