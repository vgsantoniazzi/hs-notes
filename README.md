# HIGH STAKES NOTES

This API is a RESTful API written in Rails to take care of Notes.


## API documentation

http://docs.hsnotes.apiary.io/

## Source Code

### Setup

```
bin/setup
```

### Server

```
bin/rails server
```

### Tests

```
bundle exec rspec
```

### Rubocop

```
bundle exec rubocop
```

### Live production API V1

https://hs-notes.herokuapp.com/api/v1/
