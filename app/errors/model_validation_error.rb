# frozen_string_literal: true
class ModelValidationError < ApplicationError
  def initialize(errors)
    @status = 400
    super errors.messages.to_json
  end
end
