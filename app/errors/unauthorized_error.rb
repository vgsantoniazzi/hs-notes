# frozen_string_literal: true
class UnauthorizedError < ApplicationError
  def initialize
    @status = 401
    super 'Not authorized'
  end
end
