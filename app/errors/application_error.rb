# frozen_string_literal: true
class ApplicationError < StandardError
  attr_accessor :status
end
