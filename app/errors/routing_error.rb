# frozen_string_literal: true
class RoutingError < ApplicationError
  def initialize
    @status = 404
    super('Endpoint requested not found.')
  end
end
