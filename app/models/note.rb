# frozen_string_literal: true
class Note < ApplicationRecord
  validates :title, presence: true
  validates :text, presence: true, length: { maximum: 320 }
  validates :user, presence: true

  belongs_to :user
end
