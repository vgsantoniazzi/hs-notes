# frozen_string_literal: true
class User < ApplicationRecord
  has_secure_password

  validates :name, presence: true

  validates :email, presence: true, uniqueness: true,
                    format: /\A\S+@.+\.\S+\z/, on: :create

  has_many :notes

  def generate_bearer_token
    update!(bearer_token: SecureRandom.uuid.delete('-'))
  end

  def generate_recovery_password_token
    update!(recovery_password_token: SecureRandom.uuid.delete('-').first(4))
  end
end
