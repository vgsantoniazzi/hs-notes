# frozen_string_literal: true
class RecoveryPasswordMailer < ApplicationMailer
  def send_email(user)
    @user = user
    mail(to: @user.email, subject: 'Your recovery password code.')
  end
end
