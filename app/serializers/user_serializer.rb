# frozen_string_literal: true
class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :bearer_token
end
