# frozen_string_literal: true
class ErrorsController < ApplicationController
  skip_before_action :authenticate!

  def routing
    raise RoutingError
  end
end
