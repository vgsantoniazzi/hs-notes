# frozen_string_literal: true
class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods

  before_action -> { request.format = :json }

  before_action :authenticate!

  attr_reader :current_user

  rescue_from Exception do |e|
    render json: {
      error: {
        message: e.class.to_s,
        class: e.class.to_s,
        status: 500
      }
    }, status: 500
  end

  rescue_from ActionController::ParameterMissing do |e|
    render json: {
      error: {
        message: e.message,
        class: e.class.to_s,
        status: 400
      }
    }, status: 400
  end

  rescue_from ApplicationError do |e|
    render json: {
      error: {
        message: parse(e.message),
        class: e.class.to_s,
        status: e.status
      }
    }, status: e.status
  end

  rescue_from ActiveRecord::RecordNotFound do |e|
    render json: {
      error: {
        message: e.message,
        class: e.class.to_s,
        status: 404
      }
    }, status: 404
  end

  protected

  def authenticate!
    authenticate_or_request_with_http_token('Bearer') do |token, _options|
      @current_user = User.find_by(bearer_token: token)
    end
  end

  private

  def parse(message)
    return JSON.parse(message)
  rescue JSON::ParserError
    return message
  end
end
