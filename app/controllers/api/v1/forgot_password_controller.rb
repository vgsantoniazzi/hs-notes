# frozen_string_literal: true
class API::V1::ForgotPasswordController < ApplicationController
  skip_before_action :authenticate!

  def create
    user.generate_recovery_password_token
    RecoveryPasswordMailer.send_email(user).deliver
    head :created
  end

  private

  def user
    @user ||= User.find_by!(email: params[:user][:email])
  end
end
