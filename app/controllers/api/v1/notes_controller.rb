# frozen_string_literal: true
class API::V1::NotesController < ApplicationController
  def index
    render json: notes
  end

  def show
    render json: note
  end

  def create
    note = current_user.notes.new(note_params)

    if note.save
      render json: note, status: 201
    else
      raise ModelValidationError, note.errors
    end
  end

  def update
    if note.update(note_params)
      render json: note
    else
      raise ModelValidationError, note.errors
    end
  end

  def destroy
    note.destroy
    head :no_content
  end

  private

  def note_params
    params.require(:note).permit(:title, :text)
  end

  def note
    @note ||= current_user.notes.find(params[:id])
  end

  def notes
    @notes ||= current_user.notes
  end
end
