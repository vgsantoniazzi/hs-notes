# frozen_string_literal: true
class API::V1::UsersController < ApplicationController
  skip_before_action :authenticate!, only: [:create]

  def show
    render json: current_user
  end

  def create
    user = User.new(user_params)

    if user.save
      render json: user
    else
      raise ModelValidationError, user.errors
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password)
  end
end
