# frozen_string_literal: true
class API::V1::RecoveryPasswordController < ApplicationController
  skip_before_action :authenticate!

  def create
    if user.update(password: params[:user][:password])
      user.update!(recovery_password_token: nil)
      render json: user
    else
      raise ModelValidationError, user.errors
    end
  end

  private

  def user
    @user ||= User.find_by!(
      email: params[:user][:email],
      recovery_password_token: params[:user][:recovery_password_token]
    )
  end
end
