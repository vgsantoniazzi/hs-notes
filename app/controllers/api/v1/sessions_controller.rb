# frozen_string_literal: true
class API::V1::SessionsController < ApplicationController
  skip_before_action :authenticate!, only: [:create]

  def create
    user = User.find_by(email: params[:user][:email])
    if user && user.authenticate(params[:user][:password])
      user.generate_bearer_token
      render json: user, status: 201
    else
      raise UnauthorizedError
    end
  end

  def destroy
    current_user.bearer_token = nil
    current_user.save
    head :no_content
  end
end
